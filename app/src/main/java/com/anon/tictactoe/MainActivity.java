package com.anon.tictactoe;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TTTGameEngine engine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView grid = findViewById(R.id.grid);

        // Recovering current state (i.e. after rotating device) and initializing engine
        int[] gridState = savedInstanceState == null ?
                new int[9] : savedInstanceState.getIntArray("grid_state");
        TTTGameEngine.BoardPlayer currentPlayer = savedInstanceState == null || savedInstanceState.getInt("current_player") == TTTGameEngine.BoardState.X_SQUARE ?
                TTTGameEngine.BoardPlayer.PLAYER_X : TTTGameEngine.BoardPlayer.PLAYER_O; // Player X always starts first
        boolean isLocked = savedInstanceState != null && savedInstanceState.getBoolean("grid_locked");

       View.OnClickListener onItemClickListener = v -> {
            Integer position = (Integer) v.getTag();

            if (engine.markCell(position)) { // Can't play on a played position
                grid.getAdapter().notifyItemChanged(position);
            }
        };

        engine = new TTTGameEngine(gridState, currentPlayer, isLocked, new TTTGameEngine.TTTCallbacks() {
            @Override
            public void onReset() {
                grid.getAdapter().notifyItemRangeChanged(0, 9);
                Toast.makeText(getApplicationContext(), R.string.reset, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onGameTie() {
                // TODO: could do something more than just a Toast.
                Toast.makeText(getApplicationContext(), R.string.game_tie, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onWinner(TTTGameEngine.BoardPlayer player) {
                // TODO: could do something more than just a Toast.
                int string = player == TTTGameEngine.BoardPlayer.PLAYER_X ? R.string.game_win_x : R.string.game_win_o;
                Toast.makeText(getApplicationContext(), string, Toast.LENGTH_LONG).show();
            }
        });

        // Dynamically infer what the cell height should be, based on the grid width
        grid.post(() -> grid.setAdapter(new TTTAdapter(engine, onItemClickListener, Math.min(grid.getMeasuredHeight(), grid.getMeasuredWidth()) / 3)));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putIntArray("grid_state", engine.getBoardState());
        outState.putInt("current_player", engine.getActivePlayer().id);
        outState.putBoolean("grid_locked", engine.isLocked());
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    // User has pressed the reset button
    public void reset(MenuItem item) {
        engine.reset();
    }
}
