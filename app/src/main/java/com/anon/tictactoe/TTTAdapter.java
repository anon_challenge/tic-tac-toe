package com.anon.tictactoe;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

class TTTAdapter extends RecyclerView.Adapter {

    private final TTTGameEngine gameEngine;
    private View.OnClickListener onItemClickListener;
    private int cellSize;

    public TTTAdapter(TTTGameEngine engine, View.OnClickListener onItemClickListener, int cellSize) {
        this.gameEngine = engine;
        this.onItemClickListener = onItemClickListener;
        this.cellSize = cellSize;
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ImageButton imageButton = (ImageButton) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tic_tac_toe_button, parent, false);
        imageButton.setOnClickListener(onItemClickListener);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(cellSize, cellSize);
        imageButton.setLayoutParams(lp);
        return new TTTViewHolder(imageButton);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ((TTTViewHolder) viewHolder).imageButton.setImageResource(getImageForState(gameEngine.getCellState(position)));
        ((TTTViewHolder) viewHolder).imageButton.setTag(position);
    }

    @Override
    public int getItemCount() {
        return 9;
    }

    public static class TTTViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        ImageButton imageButton;
        public TTTViewHolder(ImageButton v) {
            super(v);
            imageButton = v;
        }
    }

    private static int getImageForState(int state) {
        switch (state) {
            case 1 :
                return R.drawable.x;
            case 2:
                return R.drawable.o;
        }
        return 0;
    }
}
