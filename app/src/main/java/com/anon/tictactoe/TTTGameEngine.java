package com.anon.tictactoe;

import static com.anon.tictactoe.TTTGameEngine.BoardState.O_SQUARE;
import static com.anon.tictactoe.TTTGameEngine.BoardState.X_SQUARE;
import static com.anon.tictactoe.TTTGameEngine.BoardState.EMPTY_SQUARE;

class TTTGameEngine {
    private int[] board; // Always 9 squares, as per instructions.
    private boolean lock; // if lock is set, cannot mark the squares
    private TTTCallbacks callbacks;

    private BoardPlayer activePlayer;

    TTTGameEngine(int[] savedBoard, BoardPlayer currentPlayer, boolean isLocked, TTTCallbacks callbacks) {
        this.activePlayer = (currentPlayer == null) ? BoardPlayer.PLAYER_X : currentPlayer; // Player X starts first
        this.callbacks = callbacks;
        this.lock = isLocked;
        this.board = (savedBoard == null) ? new int[9] : savedBoard;
    }

    int getCellState(int position) {
        return board[position];
    }

    private void changeActivePlayer() {
        activePlayer = (activePlayer == BoardPlayer.PLAYER_O) ? BoardPlayer.PLAYER_X : BoardPlayer.PLAYER_O;
    }

    int[] getBoardState() {
        return board;
    }

    BoardPlayer getActivePlayer() {
        return activePlayer;
    }

    boolean markCell(Integer position) {
        if (isLocked() || position > 8 || board[position] != EMPTY_SQUARE) {
            // no-op if square isn't empty or if the board is locked
            return false;
        }

        board[position] = activePlayer.id;

        if (isGameWon(position)) {
            this.lock = true;
            callbacks.onWinner(activePlayer.id == X_SQUARE ? BoardPlayer.PLAYER_X : BoardPlayer.PLAYER_O);
        } else if (isBoardFilled()) {
            callbacks.onGameTie();
        }

        changeActivePlayer();
        return true;
    }

    private boolean isGameWon(int position) {
        // Inspect row
        int rowNumber = position / 3;
        int rowStartIndex = rowNumber * 3;
        int[] row = new int[] {rowStartIndex, rowStartIndex + 1, rowStartIndex + 2};
        if (isHomogeneousArray(row)) {
            return true;
        }

        // Inspect column
        int columnNumber = position % 3;
        int[] column = new int[] {columnNumber, columnNumber + 3, columnNumber + 6};
        if (isHomogeneousArray(column)) {
            return true;
        }

        // Check the two diagonals
        if (board[4] != EMPTY_SQUARE && isHomogeneousArray(new int[] {0, 4, 8})) {
            return true;
        }

        return board[4] != EMPTY_SQUARE && isHomogeneousArray(new int[]{2, 4, 6});
    }

    private boolean isHomogeneousArray(int[] array) {
        for (int position = 1; position < array.length; position++) {
            if (board[array[position]] != board[array[0]]) {
                return false;
            }
        }
        return true;
    }

    private boolean isBoardFilled() {
        for (int i : board) {
            if (i == EMPTY_SQUARE) {
                return false;
            }
        }
        return true;
    }

    boolean isLocked() {
        return lock;
    }

    void reset() {
        board = new int[9];
        lock = false;
        activePlayer = BoardPlayer.PLAYER_X;
        callbacks.onReset();
    }

    public interface BoardState {
        int EMPTY_SQUARE = 0;
        int X_SQUARE = 1;
        int O_SQUARE = 2;
    }

    public enum BoardPlayer {
        PLAYER_X(X_SQUARE), PLAYER_O(O_SQUARE);
        public int id;

        BoardPlayer(int id) {
            this.id = id;
        }
    }

    interface TTTCallbacks {
        void onReset();
        void onGameTie();
        void onWinner(BoardPlayer player);
    }
}
