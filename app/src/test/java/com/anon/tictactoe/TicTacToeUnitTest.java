package com.anon.tictactoe;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TicTacToeUnitTest {

    private TTTGameEngine engine;
    private TTTGameEngine.BoardPlayer winner;
    private boolean gameTie = false;

    @Before
    public void init() {
        engine = new TTTGameEngine(new int[9], null, false, new TTTGameEngine.TTTCallbacks() {
            @Override
            public void onReset() {
                winner = null;
                gameTie = false;
            }

            @Override
            public void onGameTie() {
                gameTie = true;
            }

            @Override
            public void onWinner(TTTGameEngine.BoardPlayer player) {
                winner = player;
            }
        });
    }

    @Test
    public void simple_game_A() {
        Assert.assertFalse(engine.markCell(10)); // Can't mark a cell that's outside the bounds
        Assert.assertTrue(engine.markCell(0)); // Player X
        Assert.assertEquals(engine.getBoardState()[0], TTTGameEngine.BoardState.X_SQUARE); // Ensure the first player was X

        Assert.assertTrue(engine.markCell(8)); // Player O
        Assert.assertTrue(engine.markCell(2)); // X
        Assert.assertFalse(engine.markCell(2)); // Can't mark a cell that's already been marked
        Assert.assertFalse(engine.markCell(8)); // Can't mark a cell that's already been marked

        Assert.assertTrue(engine.markCell(5)); // O
        Assert.assertTrue(engine.markCell(1)); // X

        Assert.assertEquals(winner, TTTGameEngine.BoardPlayer.PLAYER_X); // Marking row [0,1,2] should trigger a win
        Assert.assertFalse(engine.markCell(6)); // Can't mark a cell after the game is over
    }

    @Test
    public void resetGame() {
        engine.reset();

        for (int i = 0; i < engine.getBoardState().length; i++) {
            // Ensure the board has been cleared
            Assert.assertEquals(engine.getBoardState()[i], 0);
        }
    }

    @Test
    public void simple_game_B() {
        Assert.assertTrue(engine.markCell(1)); // Player X
        Assert.assertEquals(engine.getBoardState()[1], TTTGameEngine.BoardState.X_SQUARE); // Ensure the first player was X

        Assert.assertTrue(engine.markCell(0)); // Player O
        Assert.assertTrue(engine.markCell(3)); // Player X
        Assert.assertTrue(engine.markCell(2)); // O
        Assert.assertTrue(engine.markCell(8)); // X
        Assert.assertTrue(engine.markCell(7)); // O
        Assert.assertTrue(engine.markCell(6)); // X
        Assert.assertTrue(engine.markCell(5)); // O
        Assert.assertTrue(engine.markCell(4)); // X

        Assert.assertTrue(gameTie); // Ensure nobody won

        engine.reset();
    }
}