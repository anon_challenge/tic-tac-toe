# TicTacToe

Simple TicTacToe implementation for Android.

## Build instructions

Run: 

    ./gradlew build

And find the output APK's in `app/build/outputs/apk/`

Connect your device via ADB and run:

    ./gradlew install

to transfer and install the app automatically.

Run:

    ./gradlew test

To run all unit tests.
